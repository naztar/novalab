#!/bin/bash
F=src/main.c
if [ ! -f "$F" ]; then
    curl https://gitea.novalab.live/novalab/resources/raw/branch/master/marathon/testLab1.c -o $F
fi

sed -i '24i#include <bsd/string.h>' $F

sed -i 's/strcpy(result.firstName, firstNames\[rand() % NAMES_COUNT\]);/strlcpy(result.firstName, firstNames[rand() % NAMES_COUNT], sizeof(result.firstName));/g' $F

sed -i 's/strcpy(result.middleName, middleNames\[rand() % NAMES_COUNT\]);/strlcpy(result.middleName, middleNames[rand() % NAMES_COUNT], sizeof(result.middleName));/g' $F

sed -i 's/strcpy(result.lastName, lastNames\[rand() % NAMES_COUNT\]);/strlcpy(result.lastName, lastNames[rand() % NAMES_COUNT], sizeof(result.lastName));/g' $F

sh build.sh
