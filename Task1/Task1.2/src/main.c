/**
 * @mainpage
 * Загальне завдання  <br/> </b>
 * Сформувати функцию, которая генерирует структуру із залученням механізму
 * випадкової генерації даних (прикладная галузь вказана в індивідуальному
 * завданні); <br/>
 * Сформувати функцію, яка буде виводити масив структур на екран. <br/>
 *
 * @author Davydov V.
 * @date 23-feb-2016
 * @version 1.0
 */

/**
 * @file testLab1.c
 *
 * Файл с описанием структруры студента и реализации функций генерирования и вывода на экран
 *
 * @author Davydov V.
 * @date 25-aug-2017
 * @version 1.2
 */

#include <stdio.h>
#include <stdlib.h>

/**
 * Структура «Студент»
 */
struct Student {
	char firstName[20]; ///< Имя студента
	char middleName[20]; ///< Отчество студента
	char lastName[20]; ///< Фамилия студента
	int *marks; ///< Оценки студента
};

/**
 * Создание данных о студенте.
 *
 * Функция генерирует случайные ФИО на основе массивов символов для имен, отчеств, фамилий;
 * оценки генерируются случайным образом от 1 до 5, сгенерированные данные
 * заполняют поля структуры Student
 * @return структура Student с заполненными полями
 */
struct Student generateStudent(int marksCount)
{
	int const NAMES_COUNT = 3; // Количество вариаций имен/фамилий/отчеств
	char firstNames[3][20]; /* массив возможных имен */
	const char middleNames[3][20] = {
		"Ivanovich", "Petrovich", "Sidorovich"
	}; /* массив возможных отчеств */
	char lastNames[3][20] = { "Ivanov", "Petrov",
				  "Sidorov" }; /* массив возможных фамилий */

	struct Student result;
	int i;

	strcpy(firstNames[0], "Ivan");
	strcpy(firstNames[1], "Petr");
	strcpy(firstNames[2], "Sidor");

	strlcpy(result.firstName, firstNames[rand() % NAMES_COUNT],
		sizeof(result.firstName));
	strlcpy(result.middleName, middleNames[rand() % NAMES_COUNT],
		sizeof(result.middleName));
	strlcpy(result.lastName, lastNames[rand() % NAMES_COUNT],
		sizeof(result.lastName));

	result.marks = (int *)malloc(marksCount * 4);
	for (i = 0; i < marksCount; i++) {
		result.marks[i] = rand() % 5 + 1; // оценки от 1 до 5
	}
	return result;
}

/**
 * Вывод на экран содержимого массива с данными о студентах.
 *
 * Функция в цикле для всех элементов переданного массива с данными о студентах выводит их на экран
 * в следующем формате "Фамилия Имя Отчество: Оценка1 Оценка2 Оценка3 Оценка4 Оценка5"
 * @param students массив с данными о студентах, которые необходимо вывести на экран
 */
void showStudents(struct Student students[], int studentsCount, int marksCount)
{
	int i;
	int j;
	for (i = 0; i < studentsCount; i++) {
		printf("Output Student #%d information: \n", i);
		printf("%s %s %s: ", students[i].lastName,
		       students[i].firstName, students[i].middleName);
		for (j = 0; j < marksCount; j++) {
			printf("%d ", students[i].marks[j]);
		}
		printf("\n");
	}
}

/**
 * Главная функция.
 *
 * Последовательность действий:
 * <ul>
 * <li> создание массива с данными о 10 студентах </li>
 * <li> в цикле – генерация данных для каждого студента, путем вызова функции </li>
 * <li> {@link generateStudent} </li>
 * <li> вывод данных обо всех студентах на экран {@link showStudents}</li>
 * </ul>
 * @return успешный код возврата из программы (0)
 */
int main()
{
	const int STUDENTS_COUNT = 10; // Количество студентов
	const int STUDENT_MARKS_COUNT = 5; // Количество оценок у студента
	struct Student students[10];
	int i;

	for (i = 0; i < STUDENTS_COUNT; i++) {
		students[i] = generateStudent(STUDENT_MARKS_COUNT);
	}
	showStudents(students, STUDENTS_COUNT, STUDENT_MARKS_COUNT);

	return 0;
}
