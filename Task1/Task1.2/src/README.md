# Fixes #3: Task 1.2: Assembly of the first project in the presence of a “foreign” file with code
## Step by step work
1. Creating a shell script `"script.sh"` that performs actions such as:
    * Downloading a file from this [link](http://https://gitea.novalab.live/novalab/resources/raw/branch/master/marathon/testLab1.c) to the `/src` directory using `curl`
    * Rename this file fo `main.c`
    * Running the script `build.sh`
2. Creating another shell script `"build.sh"` that performs actions such as:
    * Deleting temporary directories and files:
        * `html` — directory with the resulting doxygen documentation
        * `dist` — directory with the resulting files
        * `out`  — directory with temporary compilation files
    * Running through the sources the `clang-format` and `clang-tidy` utilities
    * Compiling the main sources and creates an executable file
    * Generating doxygen documentation
3. Сhecking the resulting file for security errors, using `clang-tidy`.
4. Fixing detected errors in "script.sh" using:
    * Installing package `"libbsd-dev"`
    * Copying `main.c` file to new `main_fixed.c` file
    * Inserting required library `<bsd/string.h>` into fixed file
    * Changing and replacing some functions
5. Configuring `doxywizard` according to the training manual.
6. After creating the "Doxyfile" file, find the line `USE_MDFILE_AS_MAINPAGE` in it and set it to ` = README.md`.
7. Сreating a `.clang-format` file that contains encoding data.
8. Creating a `.gitignore` file with a list of exception files to upload to the git repository. I placed there the directories `/html`, `/out`, `/dist` and `/.git`
9. Runnig the `script.sh` script, make sure that the compilation was successful.
10. Commit changes and send pull request using `git` - commands.