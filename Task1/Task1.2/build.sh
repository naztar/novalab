#!/bin/bash
D1=html
D2=dist
D3=out
F=src/main.c
if [ -d "$D1" ]; then
    rm -rf "$D1"
fi
if [ -d "$D2" ]; then
    rm -rf "$D2"
fi
if [ -d "$D3" ]; then
    rm -rf "$D3"
fi
clang-format $F -i
clang-tidy -warnings-as-errors='*' $F

mkdir "$D2"
mkdir "$D3"
clang -lbsd -std=c11 -Wall -Wextra -Werror -Wpedantic $F -o dist/task01

doxygen src/Doxyfile
