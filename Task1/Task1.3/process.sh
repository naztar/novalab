D1=Task1.2
D2=Task1.3
F1=$D2/out/nm_out.txt
F2=$D1/dist/task01
F3=$D2/task01_stat.txt
cd ..

du -ch $D1/html/*.html
nm -u $D1/dist/task01 > $F1
grep -v " _" $F1
rm $F1

cat $F2 > $F3
chmod go-x $F2
cat $F2 >> $F3
