/**
 * @file sources.c
 * File with executable functions
 * @author Nazar Taran
 * @version 1.0.0
 * @date 2020.05.30
 */

#include "../inc/header.h"

void print_sin()
{
    char matrix[N][M];

    /**
     * Matrix initialization
     */
    for (int i = 0; i < N; i++)
        for (int j = 0; j < M; j++) {
            struct point_cartesian point;
            point.x = i;
            point.y = j;
            matrix[i][j] = ' ';
            if (j == M / 2)
                matrix[i][j] = '|';
            if (i == N - 1) {
                matrix[i][j] = '-';
                if (j == M / 2)
                    matrix[i][j] = '+';
            }
        }

    int center = N / 2;
    int j = 0;
    double amplitude  = 0.1; /// amplitude

    /**
     * Filling out
     */
    for(double i = 0; i < M * amplitude; i += amplitude, j++) {
        double s = sin(i);

        if (s < 0)
            matrix[center + (int)(s * N / 2)][j] = '*';
        else if (s > 0)
            matrix[center - 1 - (int)(-s * N / 2)][j] = '*';
        else
            matrix[N / 2][j] = '*';
    }

    /**
     * Output
     */
    for (int i = 0; i < N; i++)
    {
        for (j = 0; j < M; j++)
            printf("%c", matrix[i][j]);
        printf("\n");
    }
}

void print_segment(struct line_segment segment)
{
    char matrix[N][M];
    bool start = false;
    for (int i = 0; i < N; i++)
        for (int j = 0; j < M; j++)
        {
            struct point_cartesian point;
            point.x = i;
            point.y = j;
            matrix[i][j] = ' ';
            if (j== M / 2)
                matrix[i][j] = '|';
            if (i == N - 1)
            {
                matrix[i][j] = '-';
                if (j == M / 2)
                    matrix[i][j] = '+';
            }

            if (((segment.point_a.x == i && segment.point_a.y == j) ||
                 (segment.point_b.x == i && segment.point_b.y == j)) && !start)
                start = true;
            else if (((segment.point_a.x == i && segment.point_a.y == j) ||
                 (segment.point_b.x == i && segment.point_b.y == j)) && start)
                start = false;
            if (is_point_on_line(point, segment) && start)
                matrix[i][j] = '*';
        }

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++)
            printf("%c", matrix[i][j]);
        printf("\n");
    }
}

void print_segment_points(struct line_segment segment)
{
    char matrix[N][M];
    for (int i = 0; i < N; i++)
        for (int j = 0; j < M; j++)
        {
            if ((segment.point_a.x == i && segment.point_a.y == j) || 
                (segment.point_b.x == i && segment.point_b.y == j))
                matrix[i][j] = '*';
            else
                matrix[i][j] = ' ';
        }

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++)
            printf("%c", matrix[i][j]);
        printf("\n");
    }
}

bool is_point_on_line(struct point_cartesian point, struct line_segment segment)
{
    double vx1 = segment.point_b.x - segment.point_a.x; /// first vector X coordinate
    double vy1 = segment.point_b.y - segment.point_a.y; /// first vector Y coordinate
    double vx2 = point.x - segment.point_a.x;           /// second vector X coordinate
    double vy2 = point.y - segment.point_a.y;           /// second vector Y coordinate

    double maxA = (segment.point_a.x > segment.point_a.y ? segment.point_a.x : segment.point_a.y);
    double maxB = (segment.point_b.x > segment.point_b.y ? segment.point_b.x : segment.point_b.y);
    double range = (maxA > maxB ? maxA : maxB) / 2;

    bool result = (range >= (vx1 * vy2 - vx2 * vy1) && (vx1 * vy2 - vx2 * vy1) >= -range);
    return result;
}

double get_length(struct line_segment segment)
{
    double result = sqrt(pow(segment.point_b.x - segment.point_a.x, 2) + pow(segment.point_b.y - segment.point_a.y, 2));
    return result;
}

struct point_cartesian parse_point(char* raw_data)
{
	struct point_cartesian point;
    point.x = 0;
    point.y = 0;
    char* regexString_1 = "x=[0-9]+&y=[0-9]+$"; /// проверка на соответствие формату
    char regexString_x[10] = "x=[0-9]+";        /// поиск X
    char regexString_y[10] = "y=[0-9]+";        /// поиск Y
    int result;                                 /// результат операции поиска
    int try = 1;                                /// попытака поиска

    regex_t regex;                              /// движок поиска
    regmatch_t group[1];                        /// результаты поиска

    while (try <= 2)                            /// не более 2-х попыток
    {
        if (try == 2)                               /// при второй попытке
            regexString_1 = "y=[0-9]+&x=[0-9]+$";   /// меняем местами X и Y
        regcomp(&regex, regexString_1, REG_EXTENDED);    /// поиск
        result = regexec(&regex, raw_data, 1, group, 0);
        if (try == 1 && result == REG_NOMATCH) try++;   /// не нашли 1 раз - ищем 2 раз
        else if (try == 2 && result == REG_NOMATCH)     /// не нашли 2 раз - ошибка
        {
            fprintf(stderr, "Can't parse following line: '%s'", raw_data);
            return point;
        }
        else break;     /// нашли
    }
    regfree(&regex);    /// чиска памяти после поиска

    char cx[5], cy[5];  /// строки для записи результов
    long int x, y;      /// целочисленные значения для записи результатов
    char *p;
    regcomp(&regex, regexString_x, REG_EXTENDED);   /// поиск X
    regexec(&regex, raw_data, 1, group, 0);
    for (int i = group[0].rm_so + 2, j = 0; i < group[0].rm_eo; i++, j++)
        cx[j] = raw_data[i];          /// запись в строку сх подстроки исходной строки
    x = strtol(cx, &p, 10);     /// конвертация в число
    regfree(&regex);    /// чистка памяти после поиска

    regcomp(&regex, regexString_y, REG_EXTENDED);   /// поиск Y
    regexec(&regex, raw_data, 1, group, 0);
    for (int i = group[0].rm_so + 2, j = 0; i < group[0].rm_eo; i++, j++)
        cy[j] = raw_data[i];          /// запись в строку сх подстроки исходной строки
    y = strtol(cy, &p, 10);     /// конвертация в число
    regfree(&regex);    /// чистка памяти после поиска

    point.x = x;
    point.y = y;

    return point;
}

double radians_to_degrees(double radians)
{
    double degrees = radians * 180 / PI;
    return degrees;
}

double degrees_to_radians(double degrees)
{
    double radians = degrees * PI / 180;
    return radians;
}

struct point_polar cartesian_to_polar(struct point_cartesian point)
{
    double angle, radius;
    if (point.x != 0)
    {
        if (point.y != 0 && point.x > 0) angle = atan(point.y / point.x);           /// I and IV quarter
        else if (point.y > 0 && point.x < 0) angle = atan(point.y / point.x) + PI;  /// II quarter
        else if (point.y < 0 && point.x < 0) angle = atan(point.y / point.x) - PI;  /// III quarter
        else
        {
            if (point.x > 0) angle = 0;     /// on OX
            else angle = PI;
        }
    }
    else
    {
        if (point.y > 0) angle = PI / 2;    /// on OY
        else if (point.y < 0) angle = -PI / 2;
        else angle = 0;                     /// on center
    }
    angle = radians_to_degrees(angle);
    radius = sqrt(point.x * point.x + point.y * point.y);

    struct point_polar result;
    result.r = radius;
    result.a = angle;
    return result;
}

struct point_cartesian polar_to_cartesian(struct point_polar point)
{
    double x, y;
    point.a = degrees_to_radians(point.a);
    x = point.r * cos(point.a);
    y = point.r * sin(point.a);

    struct point_cartesian result;
    result.x = x;
    result.y = y;
    return result;
}