/**
 * @file test.c
 * File with module tests
 * @author Nazar Taran
 * @version 1.0.0
 * @date 2020.05.27
 */
#include "header.h"

Describe(Test);
BeforeEach(Test)
{
}
AfterEach(Test)
{
}

Ensure(Test, ret_point_on_line)
{
	const int tests = 4;
	double coordinates[tests][3][2] = {{{2, 1}, {23, 4}, {0, 0}}, 
								   {{2, 1}, {-2, -1}, {0, 0}},
								   {{5, -1}, {-20, -1}, {4, 10}},
								   {{2, 1}, {-2, -1}, {1.5, 1.5}}};
	bool results[tests] = {false, true, false, false};

	for (int i = 0; i < tests; i++)
	{
		struct point_cartesian pointA;
		struct point_cartesian pointB;
		struct point_cartesian pointC;
		struct line_segment line;

		pointA.x = coordinates[i][0][0];
		pointA.y = coordinates[i][0][1];
		pointB.x = coordinates[i][1][0];
		pointB.y = coordinates[i][1][1];
		pointC.x = coordinates[i][2][0];
		pointC.y = coordinates[i][2][1];
		line.point_a = pointA;
		line.point_b = pointB;

		bool result = is_point_on_line(pointC, line);
		assert_that(result, is_equal_to(results[i]));
	}
}

Ensure(Test, ret_len)
{
	const int tests = 4;
	double coordinates[tests][2][2] = {{{2, 1}, {23, 4}}, {{-2, 1}, {0, 4}}, {{1, 1}, {1, 1}}, {{6, -6}, {-6, 6}}};
	double results[tests] = {21.21, 3.6, 0, 16.97};

	for (int i = 0; i < tests; i++)
	{
		struct point_cartesian pointA;
		struct point_cartesian pointB;
		struct line_segment line;

		pointA.x = coordinates[i][0][0];
		pointA.y = coordinates[i][0][1];
		pointB.x = coordinates[i][1][0];
		pointB.y = coordinates[i][1][1];
		line.point_a = pointA;
		line.point_b = pointB;

		double length = get_length(line);
		significant_figures_for_assert_double_are(2);
		assert_that_double(length, is_equal_to_double(results[i]));
	}
}

Ensure(Test, ret_parse)
{
	const int tests = 6;
	char* strings[tests] = {"point:x=12&y=25", "point:y=12&x=25", "point:y=&x=25", "point:y=10", "point:y=10&", "point:y=10&x=5x"};			/// control values
	int results[tests][2] = {{12, 25}, {25, 12}, {0, 0}, {0, 0}, {0, 0}, {0, 0}};
	char* errors[tests] = {"", "", "Can't parse following line: 'point:y=&x=25'", "Can't parse following line: 'point:y=10'",
	 				   		   "Can't parse following line: 'point:y=10&'", "Can't parse following line: 'point:y=10&x=5x'"};

	for (int i = 0; i < tests; i++)
	{
		struct point_cartesian point;
		char buffer[150];
		FILE* file = freopen("log.txt", "w", stderr);
		point = parse_point(strings[i]);
		fclose(file);

		significant_figures_for_assert_double_are(0);
		assert_that_double(point.x, is_equal_to_double(results[i][0]));
		assert_that_double(point.y, is_equal_to_double(results[i][1]));

	    file = fopen("log.txt", "rt");
	    fgets(buffer, 150, file);
	    fclose(file);
		assert_that(buffer, begins_with_string(errors[i]));
	}
}

Ensure(Test, ret_polar)
{
	const int tests = 9;
	double cartesian_coordinates[tests][2] = {{0, 0}, {25, 0}, {-25, 0}, {0, -25}, {10, 10}, {-25, -25}, {0, 25}, {10, -10}, {-10, 10}};			/// control values
	double polar_coordinates[tests][2] = {{0, 0}, {25, 0}, {25, 180}, {25, -90}, {14.14, 45}, {35.36, -135}, {25, 90}, {14.14, -45}, {14.14, 135}};
	significant_figures_for_assert_double_are(0);
	struct point_cartesian point;
	for (int i = 0; i < tests; i++)
	{
		point.x = cartesian_coordinates[i][0];
		point.y = cartesian_coordinates[i][1];
		struct point_polar res = cartesian_to_polar(point);	/// executing test function
		assert_that_double(res.r, is_equal_to_double(polar_coordinates[i][0]));
		assert_that_double(res.a, is_equal_to_double(polar_coordinates[i][1]));
	}
}

Ensure(Test, ret_cartesian)
{
	const int tests = 9;
	double cartesian_coordinates[tests][2] = {{0, 0}, {25, 0}, {-25, 0}, {0, -25}, {10, 10}, {-25, -25}, {0, 25}, {10, -10}, {-10, 10}};			/// control values
	double polar_coordinates[tests][2] = {{0, 0}, {25, 0}, {25, 180}, {25, -90}, {14.14, 45}, {35.36, -135}, {25, 90}, {14.14, -45}, {14.14, 135}};
	significant_figures_for_assert_double_are(0);
	struct point_polar point;
	for (int i = 0; i < tests; i++)
	{
		point.r = polar_coordinates[i][0];
		point.a = polar_coordinates[i][1];
		struct point_cartesian res = polar_to_cartesian(point);	/// executing test function
		assert_that_double(res.x, is_equal_to_double(cartesian_coordinates[i][0]));
		assert_that_double(res.y, is_equal_to_double(cartesian_coordinates[i][1]));
	}
}

Ensure(Test, ret_rad)
{
	const int tests = 6;
	double radians[tests] = { 1, 0, 10, 0.17, 6.28, 4.89 };
	double deg_test[tests] = { 57.29, 0, 572.95, 9.74, 359.81, 280.17 };
	significant_figures_for_assert_double_are(2);
	for (int i = 0; i < tests; i++) {
		double deg_result = radians_to_degrees(radians[i]);
		assert_that_double(deg_result, is_equal_to_double(deg_test[i]));
	}
}

Ensure(Test, ret_deg)
{
	const int tests = 6;
	double degrees[tests] = { 57.29, 0, 212.95, 10, 360, 1000 };
	double rad_test[tests] = { 1, 0, 3.71, 0.17, 6.28, 17.45 };
	significant_figures_for_assert_double_are(2);
	for (int i = 0; i < tests; i++) {
		double deg_result = degrees_to_radians(degrees[i]);
		assert_that_double(deg_result, is_equal_to_double(rad_test[i]));
	}
}

TestSuite *test_coordinates()
{
	TestSuite *suite = create_test_suite();
	add_test_with_context(suite, Test, ret_rad);
	add_test_with_context(suite, Test, ret_deg);
	add_test_with_context(suite, Test, ret_polar);
	add_test_with_context(suite, Test, ret_cartesian);
	add_test_with_context(suite, Test, ret_parse);
	add_test_with_context(suite, Test, ret_len);
	add_test_with_context(suite, Test, ret_point_on_line);
	return suite;
}
