/**
 * @file main.h
 * File with headers
 * @author Nazar Taran
 * @version 1.0.0
 * @date 2020.05.27
 */

#pragma once

#include "../inc/header.h"
#include <cgreen/cgreen.h>

/**
 * Creates suiute for test
 * @return suite
 */
TestSuite *test_coordinates();