/**
 * @file main.c
 * File with enter point
 * @author Nazar Taran
 * @version 1.0.0
 * @date 2020.05.27
 */

#include "header.h"

int main(int argc, char **argv)
{
	TestSuite *suite = create_test_suite();
	add_suite(suite, test_coordinates());
	if (argc > 1) {
		return run_single_test(suite, argv[1], create_text_reporter());
	}
	return run_test_suite(suite, create_text_reporter());
}