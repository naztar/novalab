/**
 * @file header.h
 * File with headers
 * @author Nazar Taran
 * @version 1.0.0
 * @date 2020.05.27
 */

#pragma once

#define PI 3.14159
#define N 45
#define M 150

#include <regex.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdbool.h>

/**
 * Describe the point in the Cartesias system
 */
struct point_cartesian
{
    double x; ///< X coordinate
    double y; ///< Y coordinate
};

/**
 * Describe the point in the polar system
 */
struct point_polar
{
    double r; ///< radius
    double a; ///< angle
};

/**
 * Describe the line segment
 */
struct line_segment
{
	struct point_cartesian point_a; ///< first point
	struct point_cartesian point_b; ///< second point
};

/**
 * Converts point_cartesian to point_polar
 * @param struct point_cartesian point
 * @return struct point_polar point
 */
struct point_polar cartesian_to_polar(struct point_cartesian point);

/**
 * Converts point_polar to point_cartesian 
 * @param struct point_polar point
 * @return struct point_cartesian point
 */
struct point_cartesian polar_to_cartesian(struct point_polar point);

/**
 * Converts radians to degrees
 * @param radians convertible value
 * @return degrees
 */
double radians_to_degrees(double);

/**
 * Converts degrees to radians
 * @param degrees convertible value
 * @return radians
 */
double degrees_to_radians(double);

/**
 * Converts string with values into point_cartesian object
 * @param char* raw_data string with values
 * @return struct point_cartesian point
 */
struct point_cartesian parse_point(char* raw_data);

/**
 * Gets line_segment lenth
 * @param struct line_segment line segment
 * @return lenth
 */
double get_length(struct line_segment segment);

/**
 * Gets true if point on the line segment
 * @param struct line_segment line segment
 * @param struct point_cartesian point
 * @return bool value
 */
bool is_point_on_line(struct point_cartesian point, struct line_segment segment);

/**
 * Output points on screen
 * @param struct line_segment segment
 */
void print_segment_points(struct line_segment segment);

/**
 * Output segment on screen
 * @param struct line_segment segment
 */
void print_segment(struct line_segment segment);

/**
 * Output sinus math function
 */
void print_sin();