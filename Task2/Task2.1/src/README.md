# Task 2.8: Print `Sin(x)`
## Step by step work
1. Creating a `main.c` file, which is an entry point of program with `main` function.
2. Creating a `header.h` file with all dependences, including:
    * `print_segment_points` function
    * `print_segment` function
    * `print_sin` function
    * `is_point_on_line` function
    * `line_segment` structure with structures of `cartesian_point`
    * `get_length` function
    * `parse_point` function
    * `cartesian_point` structure with coordinates in cartesian system
    * `polar_point` structure with coordinates in polar system
    * `radians_to_degrees` function
    * `degrees_to_radians` function
    * `test_corners` test suite
3. Creating `sources.c` file with realization of functions:
    * `print_segment_points` function
    * `print_segment` function
    * `print_sin` function
    * `is_point_on_line` function
    * `get_length` function
    * `parse_point` function
    * `cartesian_to_polar` function
    * `polar_to_cartesian` function
    * `radians_to_degrees` function
    * `degrees_to_radians` function
4. Creating `test` folder with `test.c`, `header.h` and `main.c` files with testing functions using `cgreen` module:
    * `is_point_on_line` function test
    * `get_length` function test
    * `parse_point` function test
    * `cartesian_to_polar` function test
    * `polar_to_cartesian` function test
5. Creating `Makefile`, which does the following:
    * Cleans all destination folders
    * Builds main executionable file
    * Сhecks the resulting file for security errors, using `clang-tidy`
    * Generates doxygen documentation based on doxyfile in parent directory
    * Builds test executionable file and runs it